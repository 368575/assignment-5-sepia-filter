%macro dot_product 3
    movaps      %1, %2
    mulps       %1, %3
    haddps      %1, %1
    haddps      %1, %1
%endmacro

%macro set_channel 2
    cvttss2si   ecx, %2
    cmp         ecx, 255
    jbe         %%.local
    mov         ecx, -1
%%.local:
    mov         %1, cl
%endmacro


global do_sepia_simd


section .text

do_sepia_simd:
    test        rsi, rsi
    jz          .finish
    movups      xmm3, [coef + 0]
    movups      xmm4, [coef + 16]
    movups      xmm5, [coef + 32]
    movss       xmm8, [max_val]
    xor         rax, rax
    lea	        rsi, [rsi + rsi * 2]
.loop:
    movdqu      xmm6, [rdi + rax]
    pmovzxbd    xmm1, xmm6
    cvtdq2ps    xmm1, xmm1
    dot_product xmm0, xmm1, xmm3
    set_channel [rdx + rax + 2], xmm0
    dot_product xmm0, xmm1, xmm4
    set_channel [rdx + rax + 1], xmm0
    dot_product xmm0, xmm1, xmm5
    set_channel [rdx + rax], xmm0
    add	        rax, 3
    cmp	        rax, rsi
    jne	        .loop
.finish:
    ret


section .rodata

coef:       dd  0.189, 0.769, 0.393, 0
            dd  0.168, 0.686, 0.349, 0
            dd  0.131, 0.534, 0.272, 0
max_val:    dd  255.0

#include "sepia.h"
#include <stddef.h>

extern void do_sepia_simd(const struct pixel* src, size_t size, struct pixel* dst);

static void do_sepia(const struct pixel* src, size_t size, struct pixel* dst)
{
    static const float kernel[] = {
            0.393f, 0.769f, 0.189f,
            0.349f, 0.686f, 0.168f,
            0.272f, 0.534f, 0.131f,
    };

    for (size_t i = 0; i < size; ++i) {
        float red = src[i].r;
        float green = src[i].g;
        float blue = src[i].b;

        float r = red * kernel[0] + green * kernel[1] + blue * kernel[2];
        float g = red * kernel[3] + green * kernel[4] + blue * kernel[5];
        float b = red * kernel[6] + green * kernel[7] + blue * kernel[8];

        dst[i].r = (uint8_t) (r < 255.f ? r : 255.f);
        dst[i].g = (uint8_t) (g < 255.f ? g : 255.f);
        dst[i].b = (uint8_t) (b < 255.f ? b : 255.f);
    }
}

struct image image_sepia(const struct image source, bool use_simd)
{
    struct image result = {0};
    if (source.data && image_init(&result, source.width, source.height)) {
        if (use_simd)
            do_sepia_simd(source.data, source.width * source.height, result.data);
        else
            do_sepia(source.data, source.width * source.height, result.data);
    }
    return result;
}

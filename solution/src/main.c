#include "bmp.h"
#include "sepia.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main( int argc, char** argv )
{
    if ((argc < 3) || (argc > 4 )|| (argc == 4 && strcmp(argv[3], "simd") != 0)) {
        fprintf(stderr, "Usage: %s <source-image> <destination-image> [simd]\n", argv[0]);
        return EXIT_FAILURE;
    }

    bool use_simd = argc == 4;

    FILE* in = fopen(argv[1], "rb");
    if (!in) {
        perror(argv[1]);
        return EXIT_FAILURE;
    }

    struct image source = {0};
    if (from_bmp(in, &source) != READ_OK) {
        fprintf(stderr, "Error reading bmp file %s\n", argv[1]);
        fclose(in);
        return EXIT_FAILURE;
    }

    fclose(in);

    struct image sepia = image_sepia(source, use_simd);

    const int CYCLES = 10;
    double total_timing = 0;
    for (int i = 0; i < CYCLES; i++){
        clock_t begin = clock();
        struct image tmp = image_sepia(source, use_simd);
        clock_t end = clock();
        total_timing += (double) (end - begin) / CLOCKS_PER_SEC;
        image_free(&tmp);
    }
    printf("%lf", total_timing / CYCLES);

    image_free(&source);

    FILE* out = fopen(argv[2], "wb");
    if (!out) {
        perror(argv[2]);
        image_free(&sepia);
        return EXIT_FAILURE;
    }

    enum write_status status = to_bmp(out, &sepia);
    image_free(&sepia);
    fclose(out);
    if (status != WRITE_OK) {
        fprintf(stderr, "Error writing bmp file %s\n", argv[2]);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

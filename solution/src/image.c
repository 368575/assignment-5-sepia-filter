#include "image.h"
#include <stdlib.h>

bool image_init(struct image* image, uint32_t width, uint32_t height)
{
    *image = (struct image) {
        .width = width,
        .height = height,
        .data = calloc(width * height, sizeof(struct pixel))
    };
    return image->data != NULL;
}

void image_free(struct image* image)
{
    if (image->data)
        free(image->data);
}

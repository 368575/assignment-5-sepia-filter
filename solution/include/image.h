#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdbool.h>
#include <stdint.h>

struct __attribute__((packed)) pixel
{
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

bool image_init(struct image* image, uint32_t width, uint32_t height);

void image_free(struct image* image);


#endif //IMAGE_TRANSFORMER_IMAGE_H

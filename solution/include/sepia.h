#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H

#include "image.h"

struct image image_sepia(struct image source, bool use_simd);

#endif //IMAGE_TRANSFORMER_ROTATE_H
